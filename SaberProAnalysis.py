#!/usr/bin/env python
# coding: utf-8

# # Schools Analysis

# In[1]:


get_ipython().system('pip install pyspark')
get_ipython().system('pip install findspark')


# In[2]:


import findspark
findspark.init()


# In[3]:


from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType, IntegerType, FloatType
from pyspark.sql import functions as F
from pyspark.sql.functions import countDistinct
from pyspark.sql.functions import count
from pyspark.sql import Row
from pyspark.sql import HiveContext
from pyspark.sql.window import Window
from pyspark.accumulators import AccumulatorParam
from pyspark.sql.types import IntegerType
from pyspark.sql.functions import udf
import sys
import re


# ### Loading Files with Spark

# In[4]:


conf = SparkConf().setAppName("SaberProAnalysis").setMaster("local[*]").set('spark.executor.memory', '4G').set('spark.driver.memory', '4G').set('spark.driver.maxResultSize', '8G')
sc = SparkContext.getOrCreate(conf)
sqlcontext = SQLContext(sc)
sqlContextHive = HiveContext(sc)
rdd=sc.textFile('/SaberPro_Clasificacion/Colegios/*.txt')

df2006=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2006.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\t', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_GEOGRAFIA","COLE_HISTORIA","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")

df2007=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2007.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\t', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_GEOGRAFIA","COLE_HISTORIA","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")

df2008=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2008.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\t', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_GEOGRAFIA","COLE_HISTORIA","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")


df2009=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2009.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\t', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_COD_CIUDAD","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")

df2010=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2010.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\t', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_COD_CIUDAD","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_GEOGRAFIA","COLE_HISTORIA","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")


df2011=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2011.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\t', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_GEOGRAFIA","COLE_HISTORIA","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")


df2012=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2012.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\|', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_GEOGRAFIA","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_HISTORIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_CIENCIAS_SOCIALES","COLE_INGLES","COLE_CATEGORIA","CLPL_N"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")

df2013=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2013.txt') .map(lambda line: re.sub('  +', ' ', line)) .map(lambda line: re.sub('"', '', line)) .map(lambda line: re.sub(',', '', line)) .map(lambda line: re.sub('\|', ';', line)) .map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_GEOGRAFIA","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_HISTORIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_CIENCIAS_SOCIALES","COLE_INGLES","COLE_CATEGORIA","CLPL_N"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")

df20141=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-20141.txt') .map(lambda line: re.sub('  +', ' ', line))  .map(lambda line: re.sub('"', '', line))  .map(lambda line: re.sub(',', '', line))  .map(lambda line: re.sub('\t', ';', line))  .map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_CIENCIAS_SOCIALES","COLE_INGLES","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")


rdd20142=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-20142.txt') .map(lambda line: re.sub('  +', ' ', line))  .map(lambda line: re.sub('"', '', line))  .map(lambda line: re.sub(',', '', line))  .map(lambda line: re.sub('\t', ';', line))  .map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_CIENCIAS_SOCIALES","COLE_INGLES","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")


rdd2015=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2015[1-2].txt')
rdd2016=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2016[1-2].txt')
rdd2017=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2017[1-2].csv')
rdd2018=sc.textFile('/SaberPro_Clasificacion/Colegios/SB11-CLASIFI-PLANTELES-2018[1-2].txt')

sqlContextHive.sql("show databases").show()


# ### Functions

# In[5]:


def get_nscience_index(chemestry, physics, biology):
    i_sum = (float(chemestry)/10) + (float(physics)/10) + (float(biology)/10)
    divisor = 3
    nc_index = i_sum / divisor
    return nc_index

def get_human_index(socials, philosophy):
    i_socials = 2 * (float(socials)/10)
    i_philosophy = 1 * (float(philosophy)/10)
    human_index = (i_socials + i_philosophy)/3
    return human_index

def get_school_index(language, maths, nscience, human, english):
    print("lang", language)
    i_language = 1 * (float(language)/10)
    i_math = 1 * (float(maths)/10)
    i_nscience = 1 * float(nscience)
    i_human = 1 * float(human)
    i_english = (1/3)*(float(english)/10)
    divisor = 4 * (1/3)
    school_index = (i_language + i_math + i_nscience + i_human + i_english ) / divisor
    return school_index


# ### Sending data from DF to Hadoop with data normalization and Same Columns

# In[6]:


df2006 = df2006.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("COLE_ESTUDIANTES_PRESENTES") 

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2006 = df2006.withColumn("PERIODO", F.lit("20061"))
df2006 = df2006.withColumn("INDICE_INGLES", F.lit("0"))
df2006 = df2006.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2006 = df2006.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2006 = df2006.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))
                           
#udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2006.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2006 = df2006.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2006 = df2006.select(sorted(df2006.columns))

df2006.write.mode('overwrite').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[7]:


df2007 = df2007.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("COLE_ESTUDIANTES_PRESENTES") 

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2007 = df2007.withColumn("PERIODO", F.lit("20071"))
df2007 = df2007.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2007 = df2007.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2007 = df2007.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2007.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2007 = df2007.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2007 = df2007.select(sorted(df2007.columns))

df2007.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[8]:


df2008 = df2008.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("COLE_ESTUDIANTES_PRESENTES") 

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2008 = df2008.withColumn("PERIODO", F.lit("20081"))
df2008 = df2008.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2008 = df2008.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2008 = df2008.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2008.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2008 = df2008.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2008 = df2008.select(sorted(df2008.columns))

df2008.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[9]:


df2009 = df2009.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_COD_CIUDAD", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_CODMPIO_COLEGIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("COLE_ESTUDIANTES_PRESENTES") .drop("INDICE_INGLES")

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2009 = df2009.withColumn("PERIODO", F.lit("20091"))
df2009 = df2009.withColumn("INDICE_INGLES", F.lit("0"))
df2009 = df2009.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2009 = df2009.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2009 = df2009.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2009.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2009 = df2009.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2009 = df2009.select(sorted(df2009.columns))

df2009.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[10]:


df2010 = df2010.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_COD_CIUDAD", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_CODMPIO_COLEGIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("COLE_ESTUDIANTES_PRESENTES") .drop("INDICE_INGLES")

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2010 = df2010.withColumn("PERIODO", F.lit("20101"))
df2010 = df2010.withColumn("INDICE_INGLES", F.lit("0"))
df2010 = df2010.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2010 = df2010.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2010 = df2010.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2010.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2010 = df2010.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2010 = df2010.select(sorted(df2010.columns))

df2010.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[11]:


df2011 = df2011.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("COLE_ESTUDIANTES_PRESENTES") .drop("INDICE_INGLES")

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2011 = df2011.withColumn("PERIODO", F.lit("20111"))
df2011 = df2011.withColumn("INDICE_INGLES", F.lit("0"))
df2011 = df2011.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2011 = df2011.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2011 = df2011.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2011.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2011 = df2011.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2011 = df2011.select(sorted(df2011.columns))

df2011.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[12]:


df2012 = df2012.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("CLPL_N")

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2012 = df2012.withColumn("PERIODO", F.lit("20121"))
df2012 = df2012.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2012 = df2012.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2012 = df2012.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2012.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2012 = df2012.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2012 = df2012.select(sorted(df2012.columns))

df2012.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[13]:


df2013 = df2013.withColumnRenamed("COLE_CODIGO_COLEGIO", "COLE_COD_DANE") .withColumnRenamed("COLE_MPIO_COLEGIO", "COLE_MPIO_MUNICIPIO") .withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA") .drop("COLE_GEOGRAFIA") .drop("COLE_HISTORIA") .drop("CLPL_N")

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df2013 = df2013.withColumn("PERIODO", F.lit("20131"))
df2013 = df2013.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df2013 = df2013.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df2013 = df2013.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df2013.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df2013 = df2013.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df2013 = df2013.select(sorted(df2013.columns))

df2013.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[14]:


df20141 = df20141.withColumnRenamed("COLE_MATEMATICAS", "INDICE_MATEMATICAS") .withColumnRenamed("COLE_LENGUAJE", "INDICE_LECTURA_CRITICA") .withColumnRenamed("COLE_INGLES", "INDICE_INGLES") .drop("COLE_INST_JORNADA")

udf_nscience_index = F.udf(get_nscience_index, FloatType())
udf_human_index = F.udf(get_human_index, FloatType())
udf_school_index = F.udf(get_school_index, FloatType())


df20141 = df20141.withColumn("PERIODO", F.lit("20141"))
df20141 = df20141.withColumn("INDICE_C_NATURALES", udf_nscience_index("COLE_QUIMICA", "COLE_FISICA", "COLE_BIOLOGIA"))
df20141 = df20141.withColumn("INDICE_SOCIALES_CIUDADANAS", udf_human_index("COLE_CIENCIAS_SOCIALES", "COLE_FILOSOFIA"))
df20141 = df20141.withColumn("INDICE_TOTAL", udf_school_index("INDICE_LECTURA_CRITICA", "INDICE_MATEMATICAS", "INDICE_C_NATURALES", "INDICE_SOCIALES_CIUDADANAS", "INDICE_INGLES"))

# uncomment to show added colunms
# df20141.select("COLE_INST_NOMBRE","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_TOTAL").show()

# drop already used colunms
df20141 = df20141.drop("COLE_CIENCIAS_SOCIALES") .drop("COLE_QUIMICA") .drop("COLE_FISICA") .drop("COLE_BIOLOGIA") .drop("COLE_FILOSOFIA")

# sort colunms order
df20141 = df20141.select(sorted(df20141.columns))

df20141.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# ### Total number of lines in the file

# In[17]:


print("Total Schools in period 20061: ", df2006.count())
print("Total Schools in period 20071: ", df2007.count())
print("Total Schools in period 20081: ", df2008.count())
print("Total Schools in period 20091: ", df2009.count())
print("Total Schools in period 20101: ", df2010.count())
print("Total Schools in period 20111: ", df2011.count())
print("Total Schools in period 20121: ", df2012.count())
print("Total Schools in period 20131: ", df2013.count())
print("Total Schools in period 20141: ", df20141.count())

df2011.printSchema()
df2012.printSchema()
df2013.printSchema()
df20141.printSchema()


# In[ ]:


rdd20141 = rdd20141.map(lambda line: re.sub('  +', ' ', line))
rdd20141 = rdd20141.map(lambda line: re.sub('"', '', line))
rdd20141 = rdd20141.map(lambda line: re.sub(',', '', line))
rdd20141 = rdd20141.map(lambda line: re.sub('\t', ';', line))
df = rdd20141.map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_CIENCIAS_SOCIALES","COLE_INGLES","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")

df.drop("COLE_INST_JORNADA")

df.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[ ]:


rdd20142 = rdd20142.map(lambda line: re.sub('  +', ' ', line))
rdd20142 = rdd20142.map(lambda line: re.sub('"', '', line))
rdd20142 = rdd20142.map(lambda line: re.sub(',', '', line))
rdd20142 = rdd20142.map(lambda line: re.sub('\|', ';', line))
df20142 = rdd20142.map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_COD_DEPTO","COLE_DEPTO_COLEGIO","COLE_NATURALEZA","COLE_GRADO","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","MATRICULADOS_ULTIMOS_3","EVALUADOS_ULTIMOS_3","INDICE_MATEMATICAS","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_LECTURA_CRITICA","INDICE_INGLES","INDICE_TOTAL","COLE_CATEGORIA"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")

df20142 = df20142.drop("PERIODO")
df20142 = df20142.drop("COLE_COD_DEPTO")
df20142 = df20142.drop("COLE_GRADO")
df20142 = df20142.drop("MATRICULADOS_ULTIMOS_3")
df20142 = df20142.drop("EVALUADOS_ULTIMOS_3")

df.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[ ]:


rdd2015 = rdd2015.map(lambda line: re.sub('  +', ' ', line))
rdd2015 = rdd2015.map(lambda line: re.sub('"', '', line))
rdd2015 = rdd2015.map(lambda line: re.sub(',', '', line))
rdd2015 = rdd2015.map(lambda line: re.sub('\|', ';', line))
df2015 = rdd2015.map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_COD_DEPTO","COLE_DEPTO_COLEGIO","COLE_NATURALEZA","COLE_GRADO","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","MATRICULADOS_ULTIMOS_3","EVALUADOS_ULTIMOS_3","INDICE_MATEMATICAS","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_LECTURA_CRITICA","INDICE_INGLES","INDICE_TOTAL","COLE_CATEGORIA"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")

df2015 = df2015.drop("PERIODO")
df2015 = df2015.drop("COLE_COD_DEPTO")
df2015 = df2015.drop("COLE_GRADO")
df2015 = df2015.drop("MATRICULADOS_ULTIMOS_3")
df2015 = df2015.drop("EVALUADOS_ULTIMOS_3")

df.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[ ]:


rdd2016 = rdd2016.map(lambda line: re.sub('  +', ' ', line))
rdd2016 = rdd2016.map(lambda line: re.sub('"', '', line))
rdd2016 = rdd2016.map(lambda line: re.sub(',', '', line))
rdd2016 = rdd2016.map(lambda line: re.sub('\|', ';', line))
df2016 = rdd2016.map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_COD_DEPTO","COLE_DEPTO_COLEGIO","COLE_NATURALEZA","COLE_GRADO","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","MATRICULADOS_ULTIMOS_3","EVALUADOS_ULTIMOS_3","INDICE_MATEMATICAS","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_LECTURA_CRITICA","INDICE_INGLES","INDICE_TOTAL","COLE_CATEGORIA"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")

df2016 = df2016.drop("PERIODO")
df2016 = df2016.drop("COLE_COD_DEPTO")
df2016 = df2016.drop("COLE_GRADO")
df2016 = df2016.drop("MATRICULADOS_ULTIMOS_3")
df2016 = df2016.drop("EVALUADOS_ULTIMOS_3")

df.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[ ]:


rdd2017 = rdd2017.map(lambda line: re.sub('  +', ' ', line))
rdd2017 = rdd2017.map(lambda line: re.sub('"', '', line))
rdd2017 = rdd2017.map(lambda line: re.sub(',', '', line))
rdd2017 = rdd2017.map(lambda line: re.sub('\|', ';', line))
df2017 = rdd2017.map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_COD_DEPTO","COLE_DEPTO_COLEGIO","COLE_NATURALEZA","COLE_GRADO","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","MATRICULADOS_ULTIMOS_3","EVALUADOS_ULTIMOS_3","INDICE_MATEMATICAS","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_LECTURA_CRITICA","INDICE_INGLES","INDICE_TOTAL","COLE_CATEGORIA"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")

df2017 = df2017.drop("PERIODO")
df2017 = df2017.drop("COLE_COD_DEPTO")
df2017 = df2017.drop("COLE_GRADO")
df2017 = df2017.drop("MATRICULADOS_ULTIMOS_3")
df2017 = df2017.drop("EVALUADOS_ULTIMOS_3")

df.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[ ]:


rdd2018 = rdd2018.map(lambda line: re.sub('  +', ' ', line))
rdd2018 = rdd2018.map(lambda line: re.sub('\t', '', line))
rdd2018 = rdd2018.map(lambda line: re.sub('\|', ';', line))
rdd2018 = rdd2018.map(lambda line: re.sub('¬', ';', line))
df2018 = rdd2018.map(lambda x:(x.split(";"))) .toDF(["PERIODO","COLE_COD_DANE","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_MUNICIPIO","COLE_COD_DEPTO","COLE_DEPTO_COLEGIO","COLE_NATURALEZA","COLE_GRADO","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","MATRICULADOS_ULTIMOS_3","EVALUADOS_ULTIMOS_3","INDICE_MATEMATICAS","INDICE_C_NATURALES","INDICE_SOCIALES_CIUDADANAS","INDICE_LECTURA_CRITICA","INDICE_INGLES","INDICE_TOTAL","COLE_CATEGORIA"]) .filter("COLE_COD_DANE != 'COLE_COD_DANE'")

df2018 = df2018.drop("PERIODO")
df2018 = df2018.drop("COLE_COD_DEPTO")
df2018 = df2018.drop("COLE_GRADO")
df2018 = df2018.drop("MATRICULADOS_ULTIMOS_3")
df2018 = df2018.drop("EVALUADOS_ULTIMOS_3")


df.write.mode('append').option("header","true").csv("/tmp/SaberPro_Clasificacion_Output/Colegios")


# In[ ]:


from pyspark.sql import SparkSession

appName = "Python PySpark Read CSV"
master = 'local'

# Create Spark session
spark = SparkSession.builder     .master(master)     .appName(appName)     .getOrCreate()

# Convert list to data frame
df = spark.read.format('csv')                 .option('header',True)                 .option('multiLine', True)                 .load('/tmp/SaberPro_Clasificacion_Output/Colegios/*.csv')
#df.show()
print("Record count is:", df.count())


# ### Visualize some records or rows

# In[ ]:


rdd.take(10)


# ### Clean up

# In[ ]:


rdd = rdd.map(lambda line: re.sub('  +', ' ', line))
rdd = rdd.map(lambda line: re.sub('\t', ';', line))
rdd = rdd.map(lambda line: re.sub(',', ';', line))
rdd = rdd.map(lambda line: re.sub('\;\;', ';-;', line))


# In[ ]:


rdd.take(10)


# In[ ]:


df = rdd.map(lambda x:(x.split(";"))) .toDF(["COLE_CODIGO_COLEGIO","COLE_INST_NOMBRE","COLE_CODMPIO_COLEGIO","COLE_MPIO_COLEGIO","COLE_DEPTO_COLEGIO","COLE_INST_JORNADA","COLE_CALENDARIO_COLEGIO","COLE_GENEROPOBLACION","COLE_NATURALEZA","COLE_CIENCIAS_SOCIALES","COLE_QUIMICA","COLE_FISICA","COLE_BIOLOGIA","COLE_FILOSOFIA","COLE_MATEMATICAS","COLE_LENGUAJE","COLE_INGLES","COLE_GEOGRAFIA","COLE_HISTORIA","COLE_CATEGORIA","COLE_ESTUDIANTES_PRESENTES"]) .filter("COLE_CODIGO_COLEGIO != 'COLE_CODIGO_COLEGIO'")

df.printSchema()
df.show(200, False)


# In[ ]:




