#!/bin/bash
# install jupyter ubuntu
# args 1 MY_PROJECT_DIR

MY_PROJECT_DIR=${1}
MY_PROJECT_ENV=${2}

sudo apt update

sudo apt install python3-pip python3-dev

sudo -H pip3 install --upgrade pip

sudo -H pip3 install virtualenv

mkdir ~/${MY_PROJECT_DIR}

cd ~/${MY_PROJECT_DIR}

virtualenv ${MY_PROJECT_ENV}

source ${MY_PROJECT_ENV}/bin/activate

source ${MY_PROJECT_ENV}/bin/activate

jupyter notebook


