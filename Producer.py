#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().system(u'pip install kafka-python')

from kafka import KafkaProducer
from kafka import KafkaConsumer


# In[3]:


from kafka import KafkaProducer
producer = KafkaProducer(bootstrap_servers='localhost:9092')
producer.send('quickstart-events', b'Hello, World!')
producer.send('quickstart-events', key=b'message-two', value=b'This is Kafka-Python  sasa')


# In[ ]:




