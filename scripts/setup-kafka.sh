#!/bin/bash

# https://kafka.apache.org/quickstart

source "/vagrant/scripts/common.sh"

function installLocalKafka {
  echo "install kafka from local file"
  FILE=/vagrant/resources/$KAFKA_ARCHIVE
  tar -xzf $FILE -C /usr/local
  sudo curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/slf4j-simple-1.7.30.jar -O -L "https://repo1.maven.org/maven2/org/slf4j/slf4j-simple/1.7.30/slf4j-simple-1.7.30.jar"
  sudo curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/zookeeper-jute-3.6.2.jar -O -L "https://repo1.maven.org/maven2/org/apache/zookeeper/zookeeper-jute/3.6.2/zookeeper-jute-3.6.2.jar"
  sudo curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/jackson-core-2.12.1.jar -O -L "https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.12.1/jackson-core-2.12.1.jar"
  sudo curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/jackson-annotations-2.12.1.jar -O -L "https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.12.1/jackson-annotations-2.12.1.jar"
  pushd /usr/local/kafka_2.13-${KAFKA_VERSION}/libs
  sudo wget -e robots=off --cut-dirs=3 --user-agent=Mozilla/5.0 --reject="index.html*" --no-parent --recursive --relative --level=1 --no-directories https://repo1.maven.org/maven2/org/apache/kafka/kafka_2.13/${KAFKA_VERSION}/
  popd
}

function installRemoteKafka {
  echo "install kafka from remote file"
  curl ${CURL_OPTS} -o /vagrant/resources/$KAFKA_ARCHIVE -O -L $KAFKA_MIRROR_DOWNLOAD
  tar -xzf /vagrant/resources/$KAFKA_ARCHIVE -C /usr/local
  curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/slf4j-simple-1.7.30.jar -O -L "https://repo1.maven.org/maven2/org/slf4j/slf4j-simple/1.7.30/slf4j-simple-1.7.30.jar"
  curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/zookeeper-jute-3.6.2.jar -O -L "https://repo1.maven.org/maven2/org/apache/zookeeper/zookeeper-jute/3.6.2/zookeeper-jute-3.6.2.jar"
  curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/jackson-core-2.12.1.jar -O -L "https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.12.1/jackson-core-2.12.1.jar"
  curl ${CURL_OPTS} -o /usr/local/kafka_2.13-${KAFKA_VERSION}/libs/jackson-annotations-2.12.1.jar -O -L "https://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.12.1/jackson-annotations-2.12.1.jar"
  pushd /usr/local/kafka_2.13-${KAFKA_VERSION}/libs
  wget -e robots=off --cut-dirs=3 --user-agent=Mozilla/5.0 --reject="index.html*" --no-parent --recursive --relative --level=1 --no-directories https://repo1.maven.org/maven2/org/apache/kafka/kafka_2.13/${KAFKA_VERSION}/
  popd
}

function installKafka {
  if resourceExists $KAFKA_ARCHIVE; then
    installLocalKafka
  else
    installRemoteKafka
  fi
  ln -s /usr/local/kafka_2.13-${KAFKA_VERSION} ${KAFKA_HOME}
  mkdir -p ${KAFKA_HOME}/logs/history
}

echo "setup kafka"

installKafka

echo "kafka setup complete"

