#!/bin/bash

echo "installing jupyter"

cp -r /vagrant/resources/python/get-pip.py .
python3 get-pip.py
apt-get install -y python3-pip
python3 -m pip --version
python3 -m pip install jupyterlab

