#!/bin/bash

echo "start jupyter"
jupyter notebook --allow-root --no-browser --port 8888 > /home/vagrant/jupyter.log 2>&1 &
sleep 2m

echo "Jupyter token"
cat /home/vagrant/jupyter.log | grep "token=" | head -1 | awk '{print$4}' | cut -d = -f2

echo "You can run a ssh tunnel to get jupyter"
echo "ssh -i mykey vagrant@10.211.55.101 -NL 8888:localhost:8888"

