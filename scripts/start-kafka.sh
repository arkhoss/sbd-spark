#!/bin/bash


source "/vagrant/scripts/common.sh"

function startServices {
	echo "starting zookeeper and Kafka services"
  echo "start zookeper server"
  ${KAFKA_HOME}/bin/zookeeper-server-start.sh ${KAFKA_HOME}/config/zookeeper.properties &
  echo "starting Kafka service"
  ${KAFKA_HOME}/bin/kafka-server-start.sh ${KAFKA_HOME}/config/server.properties &
  echo "Create quickstart-events topic"
  ${KAFKA_HOME}/bin/kafka-topics.sh --create --topic quickstart-events --bootstrap-server localhost:9092
  echo "Check Kafka Topic quickstart-events was created"
  ${KAFKA_HOME}/bin/kafka-topics.sh --describe --topic quickstart-events --bootstrap-server localhost:9092
}

echo "setup kafka"

startServices
echo "kafka start complete"

